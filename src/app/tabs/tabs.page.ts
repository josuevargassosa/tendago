import { DataService } from './../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

  plataforma = 'movil';

  constructor(private data:DataService) {}

  ngOnInit() {
    this.plataforma =  this.data.getPlatform();
  }

}
