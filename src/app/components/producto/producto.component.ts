import { Router } from '@angular/router';
import { DataService } from './../../services/data.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss'],
})
export class ProductoComponent implements OnInit {

  plataforma;
  @Input() detalleProductoC:any;
  dataProducto;

  @Output() regresarDivisionVar: EventEmitter<any> = new EventEmitter();
  @Output() regresarInicioVar: EventEmitter<any> = new EventEmitter();

  constructor(private dataservice: DataService, private router: Router) { }

  async ngOnInit() {
    console.log(this.detalleProductoC);
    await this.dataservice.getProductosId(this.detalleProductoC).then( data => {
      this.dataProducto = data;
    });
    this.plataforma = this.dataservice.getPlatform();
  }

  async AgregarCarrito() {

    this.router.navigateByUrl('/tabs/carrito');
    
    console.log('Data producto', this.dataProducto);

    // this.dataCarrito.push(this.dataProducto)

    // const data = JSON.stringify([this.dataCarrito])

    // await Storage.set({
    //   key: '1',
    //   value: data
    // });
  }

  regresarInicio() {
    this.regresarInicioVar.emit();
  }

  regresarDivison() {
    this.regresarDivisionVar.emit();
  }

}
