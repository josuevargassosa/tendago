import { HeaderPageComponent } from './headers/header-page/header-page.component';
import { HeaderInicioComponent } from './headers/header-inicio/header-inicio.component';
import { ProductoComponent } from './producto/producto.component';
import { EditarPerfilComponent } from './editar-perfil/editar-perfil.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { IonicModule, } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DivisionesComponent } from './divisiones/divisiones.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatMenuModule} from '@angular/material/menu';



@NgModule({
  entryComponents: [
    DivisionesComponent,
    EditarPerfilComponent,
    ProductoComponent,
    HeaderInicioComponent,
    HeaderPageComponent
  ],
  declarations: [
    DivisionesComponent,
    EditarPerfilComponent,
    ProductoComponent,
    HeaderInicioComponent,
    HeaderPageComponent
  ],
  exports: [
    DivisionesComponent,
    EditarPerfilComponent,
    ProductoComponent,
    HeaderInicioComponent,
    HeaderPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxPaginationModule,
    MatMenuModule
  ]
})
export class ComponentsModule { }
