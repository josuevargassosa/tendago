import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.scss'],
})
export class EditarPerfilComponent implements OnInit {

  constructor(private modalController: ModalController, private modalService: NgbModal) { }

  ngOnInit() {

  }

  cerrarModal() {
    this.modalController.dismiss();
  }

  save() {

  }

}
