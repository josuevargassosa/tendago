import { EditarPerfilComponent } from './../../components/editar-perfil/editar-perfil.component';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  plataforma = '';
  publisherEstado = 'PEDIDOS'

  perfilSecciones:any = [
    {
      nombre: 'Dashboard'
    },
    {
      nombre: 'Editar Perfil'
    },
    {
      nombre: 'Historial Ordenes'
    },
    {
      nombre: 'Detalles Ordenes'
    },
    {
      nombre: 'Direcciones'
    },
    {
      nombre: 'Editar Direccion'
    },
    {
      nombre: 'Contrasena'
    },
    {
      nombre: 'Cerrar Session'
    },
  ]

  constructor(public platform: Platform, private dataservice: DataService,
              private router: Router, private modalController: ModalController) { }

  ngOnInit() {
    this.plataforma = this.dataservice.getPlatform();
  }


  home() {
    this.router.navigateByUrl('/tabs/inicio');
  }

  perfil() {
    this.router.navigateByUrl('/tabs/perfil');
  }

  carrito() {
    this.router.navigateByUrl('/tabs/carrito');
  }

  configuraciones() {
    
  }

  segmentChanged2(event) {
    const valorSegmento = event.detail.value;
    this.publisherEstado = valorSegmento;
    // this.subs.unsubscribe();
    // this.reservasData(this.publisherEstado);
  }

  async editarPerfil() {
    const modal = await this.modalController.create({
      component: EditarPerfilComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }


}
