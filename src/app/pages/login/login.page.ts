import { DataService } from './../../services/data.service';
import { Platform } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  platform = 'movil';

  constructor(private router: Router,
              private data: DataService) { }

  ngOnInit() {
    this.platform = this.data.getPlatform() 
    console.log(this.platform);
  }
  

  async signin() {
    this.router.navigateByUrl('/tabs/inicio');;
  }

}
