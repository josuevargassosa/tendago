import { Router } from '@angular/router';
import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Plugins } from "@capacitor/core";

const {Storage} = Plugins

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {

  productos = [
    {
      nombre: 'Camiseta negra',
      precio: 155,
      descripcion: 'La mejor camisa del mercado',
      urlImage: 'assets/images/guitarra.jpg'
    }
  ]

  options = {
    step: 1,
    min: null,
    max: null,
    disabled: false,
    readonly: false
};

  plataforma = '';
  dataCarrito;

  constructor(public platform: Platform, private dataservice: DataService,
              private router: Router) { }

  async ngOnInit() {
    this.plataforma = this.dataservice.getPlatform();

    const ret = await Storage.get({ key: '1' });
    this.dataCarrito = JSON.parse(ret.value);
    console.log('Data storage', this.dataCarrito);
  }

  home() {
    this.router.navigateByUrl('/tabs/inicio');
  }

  inicio() {
    this.router.navigateByUrl('/tabs/inicio');
  }

  perfil() {
    this.router.navigateByUrl('/tabs/perfil');
  }

  carrito() {
    this.router.navigateByUrl('/tabs/carrito');
  }

  input(): void {
    // this.onChange(this.value);
  }

  onTouched = () => {};

  // JSON "set" example
  async comprar() {
    await Storage.set({
      key: '1',
      value: JSON.stringify({
        id: 1,
        name: 'Josue Vargas',
        correo: 'josuevargass@hotmail.com'
      })
    });
  }

  // JSON "get" example
  async getObject() {
    const ret = await Storage.get({ key: 'compra' });
    this.dataCarrito = JSON.parse(ret.value);
    console.log('Data storage', this.dataCarrito);
  }




}
