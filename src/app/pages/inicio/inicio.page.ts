import { Router } from '@angular/router';
import { DataService } from './../../services/data.service';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IonSearchbar, Platform } from '@ionic/angular';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  @Output()
  propagar = new EventEmitter<string>();
  
  @Output()
  propagar2 = new EventEmitter<string>();
  @ViewChild(IonSearchbar) ionSearchbar: IonSearchbar ;

  // Variables
  segmentos2 = []
  segmentos = [1,1,1,1,1,1,1,1];
  listaproductos:any = [];
  filtroLista: any = [];
  filtro = [];
  inicio = true;
  productos = false;
  detalleProducto = false;
  lista = false;
  divisionI = '';
  detalleProductoP;
  pageActual: number = 1
  plataforma = '';
  textoBuscar = '';
  productosLista: any[] = [];
  buscando = false;
  slideOpts = {
    slidesPerView: 1,
    // freeMode: true,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  };

  constructor(private dataservice: DataService, public platform: Platform,
              private router: Router, private menu: MenuController) { }

  ngOnInit() {

    this.plataforma = this.dataservice.getPlatform();
    console.log('Home',this.platform);

    this.dataservice.getDivisiones().then( (elem:any) => {
      elem.forEach( produc => {
        this.segmentos2.push(produc);
      });
      console.log('Divisiones',this.segmentos2);
    })

    this.dataservice.getProductos().then( (elem:any) => {
      elem.forEach( produc => {
        this.listaproductos.push(produc);
      });
      console.log('Productos',this.listaproductos);
    })
  }

  input(event) {
    console.log('Ingreso',event); 
    this.inicio = false;
    this.lista = true;
    this.productos = false;
    this.filtroLista = this.listaproductos.slice(-10)
    console.log(this.filtroLista);
    this.filtro = this.listaproductos;
  }

  filterList(event) {
    console.log('filtrando',event); 
    // this.filtro = this.listaproductos;
    const searchTerm = event.srcElement.value;
    console.log(searchTerm);

    if(!searchTerm) {
      console.log('!return');
      this.filtroLista = this.filtro;
      return;

    }
    this.filtroLista = this.filtro;
    this.filtro = this.filtroLista = this.filtroLista.filter((elemt:any) => {
      if (elemt && searchTerm) {
          // console.log(elemt);
          return (elemt.Producto.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
      };
    });
  }

  productoBuscar(item) {
    console.log(item);
  }

  search(producto) {
    this.productos = true;
    console.log('value',producto);
  }

  cancelSearch() {
    this.inicio = true
    this.productos = false;
    this.lista = false;
  }


  segmentChanged(event) {
    this.divisionI = '';
    console.log(event);
    this.divisionI = event.target.value;
    console.log(this.divisionI);
  }

  regresarHome(event) {
    this.productos = false;
    this.inicio = true;
    this.lista = false;
    this.divisionI = '';
    this.detalleProducto = false;
  }

  regresarInicioP(event) {
    console.log(event);
    this.productos = false;
    this.inicio = true;
    this.lista = false;
    this.divisionI = '';
    this.detalleProducto = false;
    console.log('Click home');
  }

  verDetalles(event) {
    console.log(event);
    this.detalleProducto = true
    this.detalleProductoP = event;
  }

  mensajeHijo2(event) {
    console.log(event);
    this.detalleProducto = true
    this.detalleProductoP = event;
  }

  home() {
    this.productos = false;
    this.inicio = true;
    this.lista = false;
    this.divisionI = '';
    this.detalleProducto = false;
    console.log('Click home');
    // this.router.navigateByUrl('/tabs/inicio');
  }

  regresarInicio() {
    this.productos = false;
    this.inicio = true;
    this.lista = false;
    this.divisionI = '';
    this.detalleProducto = false;
  }

  regresarDivision() {
    this.inicio = false;
    this.detalleProducto = false;
  }

  perfil() {
    this.router.navigateByUrl('/tabs/perfil');
  }

  carrito() {
    this.router.navigateByUrl('/tabs/carrito');
  }

  buscar(event) {
    this.inicio = false;
    this.productos = false;
    this.divisionI = ''
    const valor:string = event.detail.value;
    this.buscando = true;

    if(valor.length === 0) {
      this.buscando = false;
      this.productosLista = [];
      this.inicio = true;
      this.productos = false;
      this.lista = false;
      this.divisionI == ''
      return;
    }
    
    this.dataservice.postBuscarProductos(valor).then((resp:any) => {
      console.log(resp);
      this.productosLista = resp;
      this.buscando = false;
    });
    console.log(valor);
  }

}
