import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InicioPageRoutingModule } from './inicio-routing.module';

import { InicioPage } from './inicio.page';
import {NgxPaginationModule} from 'ngx-pagination';
import { MatSliderModule } from '@angular/material/slider';
import {MatMenuModule} from '@angular/material/menu';

@NgModule({
  imports: [
    MatSliderModule,
    CommonModule,
    FormsModule,
    IonicModule,
    InicioPageRoutingModule,
    NgxPaginationModule,
    ComponentsModule,
    MatMenuModule
  ],
  declarations: [InicioPage]
})
export class InicioPageModule {}
