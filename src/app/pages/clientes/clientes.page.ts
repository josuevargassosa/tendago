import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {

  constructor(private dataService: DataService) { }

  //Variables
  clientes = [];

  ngOnInit() {
    this.getClientes();//Llamamos a la función getPost cuando la vista se cargue
  }

  getClientes() { //llamamos a la funcion getPost de nuestro servicio.
    // this.dataService.getEmpresas()
    // .then((data) => {
    //   console.log('Datos clientes', data);
    //   this.clientes.push(data) ;
    // });
  }

}
