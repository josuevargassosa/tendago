import { DataService } from './../../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Plugins } from "@capacitor/core";

const {Storage} = Plugins


@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {

  id = this.route.snapshot.paramMap.get('Id');
  plataforma;
  producto;
  dataCarrito: any[] = [];
  dataProducto;

  constructor(private router: Router, private route: ActivatedRoute, private data:DataService) { }

  async ngOnInit() {
    console.log(this.id);
    await this.data.getProductosId(this.id).then( produc => {
      console.log(produc);
      this.producto = produc;
    });
    this.plataforma = this.data.getPlatform();
    console.log(this.plataforma);


    const ret = await Storage.get({ key: '1' });
    this.dataCarrito = JSON.parse(ret.value);
    console.log('Data storage', this.dataCarrito);


    this.dataProducto = {
      id: this.id,
      nombre: this.producto.Producto
    }

    

  }

  ionViewWillEnter(){
    console.log('Id del producto', this.id);
  }


  async AgregarCarrito() {

    this.router.navigateByUrl('/tabs/carrito');
    
    console.log('Data producto', this.dataProducto);

    this.dataCarrito.push(this.dataProducto)

    const data = JSON.stringify([this.dataCarrito])

    await Storage.set({
      key: '1',
      value: data
    });
  }

}
