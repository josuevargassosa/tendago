import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  platform: string;

  constructor(private http: HttpClient) { }


  getClientes(){
    // return new Promise(resolve=>{
    //   this.http.get('https://api.binasystem.com/tendaGO/clients/search/a', {headers:{app_token:"645228AE9A064DC7A336D3830674RB53"}}).subscribe(data=>{
    //       resolve(data);
    //       console.log(data);
    //   },error=>{
    //     console.log(error);
    //   });
    // });
  }

  getDivisiones(){
    return new Promise(resolve=>{
      this.http.get('http://api.binasystem.com/tendaGO/divisions/all', {headers:{app_token:"645228AE9A064DC7A336D3830674RB53"}}).subscribe(data=>{
          resolve(data);
          // console.log(data);
      },error=>{
        console.log(error);
      });
    });
  }

  //Todos los datos
  getProductos(){
    return new Promise(resolve=>{
      this.http.get('http://api.binasystem.com/tendaGO/products/all;search=4', {headers:{app_token:"645228AE9A064DC7A336D3830674RB53"}}).subscribe(data=>{
          resolve(data);
          // console.log(data);
      },error=>{
        console.log(error);
      });
    });
  }

  getProductosId(id){
    return new Promise(resolve=>{
      this.http.get(`http://api.binasystem.com/tendaGO/products/${id}`, {headers:{app_token:"645228AE9A064DC7A336D3830674RB53"}}).subscribe(data=>{
          resolve(data);
          console.log(data);
      },error=>{
        console.log(error);
      });
    });
  }

  postBuscarProductos(producto){
    console.log('Id Division', producto);
    return new Promise(resolve=>{
      this.http.post(`http://api.binasystem.com/tendaGO/products/search?`,{
        "StateForSearch": "All",
        "SearchTerm": producto
      }, {headers:{app_token:"645228AE9A064DC7A336D3830674RB53"}}).subscribe(data=>{
          resolve(data);
          // console.log(data);
      },error=>{
        console.log(error);
      });
    });
  }

  getProductosDivision(division){
    console.log('Id Division', division);
    return new Promise(resolve=>{
      this.http.post(`http://api.binasystem.com/tendaGO/products/search?`,{
        "StateForSearch": "All",
        "SearchTerm": division
      }, {headers:{app_token:"645228AE9A064DC7A336D3830674RB53"}}).subscribe(data=>{
          resolve(data);
          // console.log(data);
      },error=>{
        console.log(error);
      });
    });
  }

  getEmpresas(){
    // return new Promise(resolve=>{
    //   this.http.get('http://api.binasystem.com/tendaGO/user/company', {headers:{app_token:"645228AE9A064DC7A336D3830674RB53"}}).subscribe(data=>{
    //       resolve(data);
    //       console.log(data);
    //   },error=>{
    //     console.log(error);
    //   });
    // });
  }

  // Variable indentificador de plataforma
  setPlatform(platform) {
    this.platform = platform
  }

  getPlatform() {
    return this.platform;
  }
}
